----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:37:50 11/19/2016 
-- Design Name: 
-- Module Name:    kyi_m2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kyi_m2 is
    Port ( a : in  STD_LOGIC_VECTOR (7 downto 0);
           out2 : out  STD_LOGIC_VECTOR (15 downto 0));
end kyi_m2;

architecture Behavioral of kyi_m2 is
signal kyi_prod, kyi_p1, kyi_p5, kyi_p6:unsigned (15 downto 0);
begin
	kyi_p1  <="0000000" & unsigned (a) & "0";
	kyi_p5  <="000" & unsigned (a) & "00000";
	kyi_p6  <="00" & unsigned (a) & "000000";
	kyi_prod <= kyi_p5 + kyi_p6 + kyi_p1;
	out2 <= std_logic_vector(kyi_prod);

end Behavioral;

