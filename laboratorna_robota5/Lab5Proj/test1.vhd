-- Vhdl test bench created from schematic D:\fpga_labs\kyi_lab3\kyi_sch.sch - Sat Nov 19 19:42:01 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY kyi_sch_kyi_sch_sch_tb IS
END kyi_sch_kyi_sch_sch_tb;
ARCHITECTURE behavioral OF kyi_sch_kyi_sch_sch_tb IS 

   COMPONENT kyi_sch
   PORT( op1	:	OUT	STD_LOGIC_VECTOR (16 DOWNTO 0); 
          op2	:	OUT	STD_LOGIC; 
          op3	:	OUT	STD_LOGIC; 
          kyi_in	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          C	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          CLR	:	IN	STD_LOGIC; 
          op4	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL op1	:	STD_LOGIC_VECTOR (16 DOWNTO 0);
   SIGNAL op2	:	STD_LOGIC;
   SIGNAL op3	:	STD_LOGIC;
   SIGNAL kyi_in	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"00";
   SIGNAL C	:	STD_LOGIC:='0';
   SIGNAL CE	:	STD_LOGIC:='1';
   SIGNAL CLR	:	STD_LOGIC:='0';
   SIGNAL op4	:	STD_LOGIC_VECTOR (15 DOWNTO 0);

BEGIN

   UUT: kyi_sch PORT MAP(
		op1 => op1, 
		op2 => op2, 
		op3 => op3, 
		kyi_in => kyi_in, 
		C => C, 
		CE => CE, 
		CLR => CLR, 
		op4 => op4
   );
clk_process : PROCESS
	BEGIN
		C <='0';
		WAIT FOR 10 ns;
		C <= '1';
		WAIT FOR 10 ns;
		END PROCESS;
		data_change_process : PROCESS
		VARIABLE I : INTEGER;
		BEGIN
		for i in 0 to 3 loop 
		WAIT FOR 20 ns; 
		kyi_in <= conv_std_logic_vector(conv_integer(kyi_in) + 1,8); 
		end loop; 
		
		kyi_in<="00000000"; 
		for i in 0 to 4 loop 
		wait for 20 ns; 
		end loop; 
		
		for i in 0 to 3 loop 
		WAIT FOR 20 ns; 
		kyi_in <= conv_std_logic_vector(conv_integer(kyi_in) + 2,8); 
		end loop; 
		
		kyi_in<="00000000"; 
		for i in 0 to 4 loop 
		wait for 20 ns; 
	end loop; 
	END PROCESS;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***
END;
