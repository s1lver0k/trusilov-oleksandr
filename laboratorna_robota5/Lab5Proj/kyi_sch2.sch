<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="kui_1(7:0)" />
        <signal name="CE" />
        <signal name="C" />
        <signal name="CLR" />
        <signal name="res(15:0)" />
        <signal name="XLXN_6" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11(7:0)" />
        <signal name="XLXN_12(7:0)" />
        <signal name="XLXN_13(15:0)" />
        <signal name="XLXN_14(15:0)" />
        <signal name="XLXN_15(15:0)" />
        <signal name="XLXN_16(15:0)" />
        <signal name="XLXN_17(15:0)" />
        <port polarity="Input" name="kui_1(7:0)" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="C" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="res(15:0)" />
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="kyi_m1">
            <timestamp>2016-11-19T14:39:37</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="kyi_m2">
            <timestamp>2016-11-19T14:39:30</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_1">
            <blockpin signalname="C" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="kui_1(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_11(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_2">
            <blockpin signalname="C" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_11(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_12(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_3">
            <blockpin signalname="C" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_17(15:0)" name="D(15:0)" />
            <blockpin signalname="res(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="kyi_m1" name="XLXI_4">
            <blockpin signalname="kui_1(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_13(15:0)" name="out1(15:0)" />
        </block>
        <block symbolname="kyi_m2" name="XLXI_5">
            <blockpin signalname="XLXN_11(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_14(15:0)" name="out2(15:0)" />
        </block>
        <block symbolname="kyi_m2" name="XLXI_6">
            <blockpin signalname="XLXN_12(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_15(15:0)" name="out2(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_7">
            <blockpin signalname="XLXN_13(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_14(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_10" name="CI" />
            <blockpin signalname="XLXN_6" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_16(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_8">
            <blockpin signalname="XLXN_15(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_16(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_6" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_17(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_11">
            <blockpin signalname="XLXN_10" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="736" y="656" name="XLXI_1" orien="R0" />
        <instance x="1264" y="640" name="XLXI_2" orien="R0" />
        <instance x="2656" y="1248" name="XLXI_3" orien="R0" />
        <branch name="kui_1(7:0)">
            <wire x2="640" y1="400" y2="400" x1="432" />
            <wire x2="736" y1="400" y2="400" x1="640" />
            <wire x2="640" y1="400" y2="976" x1="640" />
            <wire x2="784" y1="976" y2="976" x1="640" />
        </branch>
        <branch name="CE">
            <wire x2="688" y1="464" y2="464" x1="592" />
            <wire x2="688" y1="464" y2="688" x1="688" />
            <wire x2="1168" y1="688" y2="688" x1="688" />
            <wire x2="688" y1="688" y2="1056" x1="688" />
            <wire x2="2656" y1="1056" y2="1056" x1="688" />
            <wire x2="736" y1="464" y2="464" x1="688" />
            <wire x2="1168" y1="448" y2="688" x1="1168" />
            <wire x2="1264" y1="448" y2="448" x1="1168" />
        </branch>
        <branch name="C">
            <wire x2="656" y1="528" y2="528" x1="576" />
            <wire x2="656" y1="528" y2="672" x1="656" />
            <wire x2="1184" y1="672" y2="672" x1="656" />
            <wire x2="656" y1="672" y2="1120" x1="656" />
            <wire x2="2656" y1="1120" y2="1120" x1="656" />
            <wire x2="736" y1="528" y2="528" x1="656" />
            <wire x2="1184" y1="512" y2="672" x1="1184" />
            <wire x2="1264" y1="512" y2="512" x1="1184" />
        </branch>
        <branch name="CLR">
            <wire x2="672" y1="624" y2="624" x1="608" />
            <wire x2="672" y1="624" y2="656" x1="672" />
            <wire x2="1152" y1="656" y2="656" x1="672" />
            <wire x2="672" y1="656" y2="1216" x1="672" />
            <wire x2="2656" y1="1216" y2="1216" x1="672" />
            <wire x2="736" y1="624" y2="624" x1="672" />
            <wire x2="1152" y1="608" y2="656" x1="1152" />
            <wire x2="1264" y1="608" y2="608" x1="1152" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="3072" y1="992" y2="992" x1="3040" />
        </branch>
        <iomarker fontsize="28" x="3072" y="992" name="res(15:0)" orien="R0" />
        <iomarker fontsize="28" x="592" y="464" name="CE" orien="R180" />
        <iomarker fontsize="28" x="576" y="528" name="C" orien="R180" />
        <iomarker fontsize="28" x="608" y="624" name="CLR" orien="R180" />
        <instance x="1168" y="1872" name="XLXI_7" orien="R0" />
        <instance x="1952" y="1872" name="XLXI_8" orien="R0" />
        <branch name="XLXN_6">
            <wire x2="1776" y1="1808" y2="1808" x1="1616" />
            <wire x2="1776" y1="1424" y2="1808" x1="1776" />
            <wire x2="1952" y1="1424" y2="1424" x1="1776" />
        </branch>
        <instance x="1888" y="1008" name="XLXI_6" orien="R0">
        </instance>
        <instance x="1264" y="1008" name="XLXI_5" orien="R0">
        </instance>
        <instance x="784" y="1008" name="XLXI_4" orien="R0">
        </instance>
        <instance x="848" y="1904" name="XLXI_11" orien="R0" />
        <branch name="XLXN_10">
            <wire x2="1168" y1="1424" y2="1424" x1="912" />
            <wire x2="912" y1="1424" y2="1776" x1="912" />
        </branch>
        <iomarker fontsize="28" x="432" y="400" name="kui_1(7:0)" orien="R180" />
        <branch name="XLXN_11(7:0)">
            <wire x2="1184" y1="400" y2="400" x1="1120" />
            <wire x2="1216" y1="400" y2="400" x1="1184" />
            <wire x2="1216" y1="400" y2="976" x1="1216" />
            <wire x2="1264" y1="976" y2="976" x1="1216" />
            <wire x2="1184" y1="384" y2="400" x1="1184" />
            <wire x2="1264" y1="384" y2="384" x1="1184" />
        </branch>
        <branch name="XLXN_12(7:0)">
            <wire x2="1760" y1="384" y2="384" x1="1648" />
            <wire x2="1760" y1="384" y2="976" x1="1760" />
            <wire x2="1888" y1="976" y2="976" x1="1760" />
        </branch>
        <branch name="XLXN_13(15:0)">
            <wire x2="1200" y1="1072" y2="1072" x1="1088" />
            <wire x2="1088" y1="1072" y2="1552" x1="1088" />
            <wire x2="1168" y1="1552" y2="1552" x1="1088" />
            <wire x2="1200" y1="976" y2="976" x1="1168" />
            <wire x2="1200" y1="976" y2="1072" x1="1200" />
        </branch>
        <branch name="XLXN_14(15:0)">
            <wire x2="1648" y1="1168" y2="1168" x1="960" />
            <wire x2="960" y1="1168" y2="1680" x1="960" />
            <wire x2="1168" y1="1680" y2="1680" x1="960" />
            <wire x2="1648" y1="976" y2="1168" x1="1648" />
        </branch>
        <branch name="XLXN_15(15:0)">
            <wire x2="1888" y1="1360" y2="1552" x1="1888" />
            <wire x2="1952" y1="1552" y2="1552" x1="1888" />
            <wire x2="2352" y1="1360" y2="1360" x1="1888" />
            <wire x2="2352" y1="976" y2="976" x1="2272" />
            <wire x2="2352" y1="976" y2="1360" x1="2352" />
        </branch>
        <branch name="XLXN_16(15:0)">
            <wire x2="1760" y1="1616" y2="1616" x1="1616" />
            <wire x2="1760" y1="1616" y2="1680" x1="1760" />
            <wire x2="1952" y1="1680" y2="1680" x1="1760" />
        </branch>
        <branch name="XLXN_17(15:0)">
            <wire x2="2528" y1="1616" y2="1616" x1="2400" />
            <wire x2="2528" y1="992" y2="1616" x1="2528" />
            <wire x2="2656" y1="992" y2="992" x1="2528" />
        </branch>
    </sheet>
</drawing>