-- Vhdl test bench created from schematic D:\fgpa_labs\KhYuI_lab1\KhYuI_sch1.sch - Sat Sep 24 13:39:37 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KhYuI_sch1_KhYuI_sch1_sch_tb IS
END KhYuI_sch1_KhYuI_sch1_sch_tb;
ARCHITECTURE behavioral OF KhYuI_sch1_KhYuI_sch1_sch_tb IS 

   COMPONENT KhYuI_sch1
   PORT( KhYuI_d	:	IN	STD_LOGIC; 
          KhYuI_e	:	IN	STD_LOGIC; 
          KhYuI_a	:	IN	STD_LOGIC; 
          KhYuI_b	:	IN	STD_LOGIC; 
          KhYuI_res	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL KhYuI_d	:	STD_LOGIC:='0';
   SIGNAL KhYuI_e	:	STD_LOGIC:='0';
   SIGNAL KhYuI_a	:	STD_LOGIC:='0';
   SIGNAL KhYuI_b	:	STD_LOGIC:='0';
   SIGNAL KhYuI_res	:	STD_LOGIC;

BEGIN

   UUT: KhYuI_sch1 PORT MAP(
		KhYuI_d => KhYuI_d, 
		KhYuI_e => KhYuI_e, 
		KhYuI_a => KhYuI_a, 
		KhYuI_b => KhYuI_b, 
		KhYuI_res => KhYuI_res
   );
	KhYuI_a<=not KhYuI_a after 10 ns;
	KhYuI_b<=not KhYuI_b after 20 ns;
	KhYuI_d<=not KhYuI_d after 40 ns;
	KhYuI_e<=not KhYuI_e after 80 ns;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
