<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KhYuI_d" />
        <signal name="KhYuI_e" />
        <signal name="KhYuI_a" />
        <signal name="KhYuI_b" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="KhYuI_res" />
        <port polarity="Input" name="KhYuI_d" />
        <port polarity="Input" name="KhYuI_e" />
        <port polarity="Input" name="KhYuI_a" />
        <port polarity="Input" name="KhYuI_b" />
        <port polarity="Output" name="KhYuI_res" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="KhYuI_b" name="I0" />
            <blockpin signalname="KhYuI_a" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="KhYuI_b" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="XLXN_7" name="I0" />
            <blockpin signalname="KhYuI_a" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_5">
            <blockpin signalname="KhYuI_e" name="I0" />
            <blockpin signalname="KhYuI_d" name="I1" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_6">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="KhYuI_res" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="864" y="576" name="XLXI_1" orien="R0" />
        <instance x="560" y="1136" name="XLXI_2" orien="R0" />
        <instance x="960" y="1168" name="XLXI_3" orien="R0" />
        <instance x="1344" y="848" name="XLXI_4" orien="R0" />
        <instance x="1296" y="1520" name="XLXI_5" orien="R0" />
        <instance x="1872" y="1168" name="XLXI_6" orien="R0" />
        <branch name="KhYuI_d">
            <wire x2="1296" y1="1392" y2="1392" x1="432" />
        </branch>
        <branch name="KhYuI_e">
            <wire x2="1296" y1="1456" y2="1456" x1="432" />
        </branch>
        <branch name="KhYuI_a">
            <wire x2="512" y1="448" y2="448" x1="384" />
            <wire x2="864" y1="448" y2="448" x1="512" />
            <wire x2="512" y1="448" y2="1040" x1="512" />
            <wire x2="960" y1="1040" y2="1040" x1="512" />
        </branch>
        <branch name="KhYuI_b">
            <wire x2="448" y1="512" y2="512" x1="384" />
            <wire x2="864" y1="512" y2="512" x1="448" />
            <wire x2="448" y1="512" y2="1104" x1="448" />
            <wire x2="560" y1="1104" y2="1104" x1="448" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="960" y1="1104" y2="1104" x1="784" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1232" y1="480" y2="480" x1="1120" />
            <wire x2="1232" y1="480" y2="720" x1="1232" />
            <wire x2="1344" y1="720" y2="720" x1="1232" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1280" y1="1072" y2="1072" x1="1216" />
            <wire x2="1280" y1="784" y2="1072" x1="1280" />
            <wire x2="1344" y1="784" y2="784" x1="1280" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1728" y1="752" y2="752" x1="1600" />
            <wire x2="1728" y1="752" y2="1040" x1="1728" />
            <wire x2="1872" y1="1040" y2="1040" x1="1728" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1712" y1="1424" y2="1424" x1="1552" />
            <wire x2="1712" y1="1104" y2="1424" x1="1712" />
            <wire x2="1872" y1="1104" y2="1104" x1="1712" />
        </branch>
        <branch name="KhYuI_res">
            <wire x2="2320" y1="1072" y2="1072" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="384" y="448" name="KhYuI_a" orien="R180" />
        <iomarker fontsize="28" x="432" y="1392" name="KhYuI_d" orien="R180" />
        <iomarker fontsize="28" x="432" y="1456" name="KhYuI_e" orien="R180" />
        <iomarker fontsize="28" x="2320" y="1072" name="KhYuI_res" orien="R0" />
        <iomarker fontsize="28" x="384" y="512" name="KhYuI_b" orien="R180" />
    </sheet>
</drawing>