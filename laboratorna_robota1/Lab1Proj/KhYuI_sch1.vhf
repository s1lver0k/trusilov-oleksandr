--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : KhYuI_sch1.vhf
-- /___/   /\     Timestamp : 09/28/2016 11:28:48
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family virtex4 -flat -suppress -vhdl D:/fpga_labs/KhYuI_lab1/KhYuI_sch1.vhf -w D:/fpga_labs/KhYuI_lab1/KhYuI_sch1.sch
--Design Name: KhYuI_sch1
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity KhYuI_sch1 is
   port ( KhYuI_a   : in    std_logic; 
          KhYuI_b   : in    std_logic; 
          KhYuI_d   : in    std_logic; 
          KhYuI_e   : in    std_logic; 
          KhYuI_res : out   std_logic);
end KhYuI_sch1;

architecture BEHAVIORAL of KhYuI_sch1 is
   attribute BOX_TYPE   : string ;
   signal XLXN_7    : std_logic;
   signal XLXN_8    : std_logic;
   signal XLXN_9    : std_logic;
   signal XLXN_10   : std_logic;
   signal XLXN_11   : std_logic;
   component NAND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NAND2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : NAND2
      port map (I0=>KhYuI_b,
                I1=>KhYuI_a,
                O=>XLXN_8);
   
   XLXI_2 : INV
      port map (I=>KhYuI_b,
                O=>XLXN_7);
   
   XLXI_3 : OR2
      port map (I0=>XLXN_7,
                I1=>KhYuI_a,
                O=>XLXN_9);
   
   XLXI_4 : AND2
      port map (I0=>XLXN_9,
                I1=>XLXN_8,
                O=>XLXN_10);
   
   XLXI_5 : XOR2
      port map (I0=>KhYuI_e,
                I1=>KhYuI_d,
                O=>XLXN_11);
   
   XLXI_6 : XOR2
      port map (I0=>XLXN_11,
                I1=>XLXN_10,
                O=>KhYuI_res);
   
end BEHAVIORAL;


