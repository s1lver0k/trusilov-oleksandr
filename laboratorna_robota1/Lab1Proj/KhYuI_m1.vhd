----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:55:22 09/24/2016 
-- Design Name: 
-- Module Name:    KhYuI_m1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KhYuI_m1 is
    Port ( KhYuI_a : in  STD_LOGIC;
           KhYuI_b : in  STD_LOGIC;
           KhYuI_d : in  STD_LOGIC;
           KhYuI_e : in  STD_LOGIC;
           KhYuI_res : out  STD_LOGIC);
end KhYuI_m1;

architecture Behavioral of KhYuI_m1 is

begin

KhYuI_res <= (( not (KhYuI_a and KhYuI_b)) and ( not KhYuI_b or KhYuI_a)) xor (KhYuI_d xor KhYuI_e);

end Behavioral;

