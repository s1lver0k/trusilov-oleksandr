----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:52:01 12/16/2017 
-- Design Name: 
-- Module Name:    l_3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity l_3 is
    Port ( l_a : in  STD_LOGIC_VECTOR (7 downto 0);
           l_out1 : out  STD_LOGIC_VECTOR (39 downto 0));
end l_3;

architecture Behavioral of l_3 is
signal l_prod,l_p30,l_p29,l_p28,l_p27,l_p26,l_p25,l_p24,l_p23,l_p22,l_p19,l_p18,l_p17,l_p13,
l_p12,l_p11,l_p10,l_p9,l_p3,l_p0:unsigned (39 downto 0);
begin
l_p0  <="00000000000000000000000000000000" & unsigned (l_A) & "";
l_p3  <="00000000000000000000000000000" & unsigned (l_A) & "000";
l_p9  <="00000000000000000000000" & unsigned (l_A) & "000000000";
l_p10 <="0000000000000000000000" & unsigned (l_A) & "0000000000";
l_p11 <="000000000000000000000" & unsigned (l_A) & "00000000000";
l_p12 <="00000000000000000000" & unsigned (l_A) & "000000000000";
l_p13 <="0000000000000000000" & unsigned (l_A) & "0000000000000";
l_p17 <="000000000000000" & unsigned (l_A) & "00000000000000000";
l_p18 <="00000000000000" & unsigned (l_A) & "000000000000000000";
l_p19 <="0000000000000" & unsigned (l_A) & "0000000000000000000";
l_p22 <="0000000000" & unsigned (l_A) & "0000000000000000000000";
l_p23 <="000000000" & unsigned (l_A) & "00000000000000000000000";
l_p24 <="00000000" & unsigned (l_A) & "000000000000000000000000";
l_p25 <="0000000" & unsigned (l_A) & "0000000000000000000000000";
l_p26 <="000000" & unsigned (l_A) & "00000000000000000000000000";
l_p27 <="00000" & unsigned (l_A) & "000000000000000000000000000";
l_p28 <="0000" & unsigned (l_A) & "0000000000000000000000000000";
l_p29 <="000" & unsigned (l_A) & "00000000000000000000000000000";
l_p30 <="00" & unsigned (l_A) & "000000000000000000000000000000";
l_prod <= (l_p0 + l_p3 + l_p9 + l_p10 + l_p11 + l_p12 + l_p13 + l_p17 + l_p18 + l_p19 + l_p22 + l_p23  +l_p24 + l_p25 + l_p26+ l_p27+ l_p28+ l_p29+ l_p30);
l_out1 <= std_logic_vector (l_prod);
end Behavioral;

