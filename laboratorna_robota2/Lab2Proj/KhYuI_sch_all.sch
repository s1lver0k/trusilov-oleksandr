<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="KhYuI_A(7:0)" />
        <signal name="KhYuI_B(7:0)" />
        <signal name="KhYuI_P1(15:0)" />
        <signal name="KhYuI_P2(15:0)" />
        <signal name="KhYuI_P3(15:0)" />
        <signal name="XLXN_15(7:0)" />
        <signal name="XLXN_16(7:0)" />
        <signal name="XLXN_17(7:0)" />
        <signal name="XLXN_18(7:0)" />
        <signal name="XLXN_20(7:0)" />
        <signal name="XLXN_21(7:0)" />
        <signal name="XLXN_22(7:0)" />
        <signal name="XLXN_23(7:0)" />
        <port polarity="Input" name="KhYuI_A(7:0)" />
        <port polarity="Input" name="KhYuI_B(7:0)" />
        <port polarity="Output" name="KhYuI_P1(15:0)" />
        <port polarity="Output" name="KhYuI_P2(15:0)" />
        <port polarity="Output" name="KhYuI_P3(15:0)" />
        <blockdef name="KhYuI_mult1">
            <timestamp>2016-10-15T18:58:37</timestamp>
            <rect width="304" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-108" height="24" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
        </blockdef>
        <blockdef name="KhYuI_mult2">
            <timestamp>2016-10-15T18:59:6</timestamp>
            <rect width="320" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-108" height="24" />
            <line x2="448" y1="-96" y2="-96" x1="384" />
        </blockdef>
        <blockdef name="KhYuI_mult3">
            <timestamp>2016-10-15T19:19:34</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="KhYuI_mult1" name="XLXI_4">
            <blockpin signalname="KhYuI_A(7:0)" name="KhYuI_A(7:0)" />
            <blockpin signalname="KhYuI_B(7:0)" name="KhYuI_B(7:0)" />
            <blockpin signalname="KhYuI_P1(15:0)" name="KhYuI_P(15:0)" />
        </block>
        <block symbolname="KhYuI_mult2" name="XLXI_5">
            <blockpin signalname="KhYuI_A(7:0)" name="KhYuI_A(7:0)" />
            <blockpin signalname="KhYuI_B(7:0)" name="KhYuI_B(7:0)" />
            <blockpin signalname="KhYuI_P2(15:0)" name="KhYuI_mP(15:0)" />
        </block>
        <block symbolname="KhYuI_mult3" name="XLXI_7">
            <blockpin signalname="KhYuI_A(7:0)" name="a(7:0)" />
            <blockpin signalname="KhYuI_B(7:0)" name="b(7:0)" />
            <blockpin signalname="KhYuI_P3(15:0)" name="p(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1248" y="1296" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1248" y="1488" name="XLXI_5" orien="R0">
        </instance>
        <branch name="KhYuI_A(7:0)">
            <wire x2="928" y1="1200" y2="1200" x1="688" />
            <wire x2="928" y1="1200" y2="1392" x1="928" />
            <wire x2="1248" y1="1392" y2="1392" x1="928" />
            <wire x2="928" y1="1392" y2="1584" x1="928" />
            <wire x2="1184" y1="1584" y2="1584" x1="928" />
            <wire x2="1248" y1="1200" y2="1200" x1="928" />
        </branch>
        <branch name="KhYuI_B(7:0)">
            <wire x2="800" y1="1264" y2="1264" x1="688" />
            <wire x2="800" y1="1264" y2="1456" x1="800" />
            <wire x2="1248" y1="1456" y2="1456" x1="800" />
            <wire x2="800" y1="1456" y2="1648" x1="800" />
            <wire x2="1184" y1="1648" y2="1648" x1="800" />
            <wire x2="1248" y1="1264" y2="1264" x1="800" />
        </branch>
        <branch name="KhYuI_P1(15:0)">
            <wire x2="1712" y1="1200" y2="1200" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="1200" name="KhYuI_P1(15:0)" orien="R0" />
        <branch name="KhYuI_P2(15:0)">
            <wire x2="1728" y1="1392" y2="1392" x1="1696" />
        </branch>
        <iomarker fontsize="28" x="1728" y="1392" name="KhYuI_P2(15:0)" orien="R0" />
        <branch name="KhYuI_P3(15:0)">
            <wire x2="1808" y1="1584" y2="1584" x1="1760" />
        </branch>
        <iomarker fontsize="28" x="1808" y="1584" name="KhYuI_P3(15:0)" orien="R0" />
        <iomarker fontsize="28" x="688" y="1200" name="KhYuI_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="688" y="1264" name="KhYuI_B(7:0)" orien="R180" />
        <instance x="1184" y="1504" name="XLXI_7" orien="R0">
        </instance>
    </sheet>
</drawing>