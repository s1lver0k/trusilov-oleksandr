----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:56:59 10/15/2016 
-- Design Name: 
-- Module Name:    KhYuI_mult2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 

entity KhYuI_mult2 is
    Port ( KhYuI_A : in  STD_LOGIC_VECTOR (7 downto 0);
           KhYuI_B : in  STD_LOGIC_VECTOR (7 downto 0);
           KhYuI_mP : out  STD_LOGIC_VECTOR (15 downto 0));
end KhYuI_mult2;

architecture Behavioral of KhYuI_mult2 is

constant WIDTH: integer:=8; 
signal khyui_ua, khyui_bv0, khyui_bv1, khyui_bv2, khyui_bv3, khyui_bv4, khyui_bv5, khyui_bv6, khyui_bv7 : unsigned (WIDTH - 1 downto 0); 
signal khyui_p0, khyui_p1, khyui_p2, khyui_p3, khyui_p4, khyui_p5, khyui_p6, khyui_p7: unsigned (WIDTH downto 0); 
signal khyui_product: unsigned (2*WIDTH - 1 downto 0); 
begin 
khyui_ua <= unsigned (KhYuI_A); 
khyui_bv0 <= (others => KhYuI_B(0)); 
khyui_bv1 <= (others => KhYuI_B(1)); 
khyui_bv2 <= (others => KhYuI_B(2)); 
khyui_bv3 <= (others => KhYuI_B(3)); 
khyui_bv4 <= (others => KhYuI_B(4)); 
khyui_bv5 <= (others => KhYuI_B(5)); 
khyui_bv6 <= (others => KhYuI_B(6)); 
khyui_bv7 <= (others => KhYuI_B(7)); 
khyui_p0 <= "0" & (khyui_bv0 and khyui_ua); 
khyui_p1 <= ("0" & khyui_p0(WIDTH DOWNTO 1)) + ("0" & (khyui_bv1 and khyui_ua)); 
khyui_p2 <= ("0" & khyui_p1(WIDTH DOWNTO 1)) + ("0" & (khyui_bv2 and khyui_ua)); 
khyui_p3 <= ("0" & khyui_p2(WIDTH DOWNTO 1)) + ("0" & (khyui_bv3 and khyui_ua)); 
khyui_p4 <= ("0" & khyui_p3(WIDTH DOWNTO 1)) + ("0" & (khyui_bv4 and khyui_ua)); 
khyui_p5 <= ("0" & khyui_p4(WIDTH DOWNTO 1)) + ("0" & (khyui_bv5 and khyui_ua)); 
khyui_p6 <= ("0" & khyui_p5(WIDTH DOWNTO 1)) + ("0" & (khyui_bv6 and khyui_ua)); 
khyui_p7 <= ("0" & khyui_p6(WIDTH DOWNTO 1)) + ("0" & (khyui_bv7 and khyui_ua)); 
khyui_product <= khyui_p7 & khyui_p6(0) & khyui_p5(0)& khyui_p4(0)& khyui_p3(0)& khyui_p2(0)& khyui_p1(0)& khyui_p0(0); 
KhYuI_mP <= std_logic_vector (khyui_product); 



end Behavioral;

