----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:45:36 10/15/2016 
-- Design Name: 
-- Module Name:    KhYuI_mult1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.NUMERIC_STD.ALL; 


 
entity KhYuI_mult1 is 
Port ( KhYuI_A : in STD_LOGIC_VECTOR (7 downto 0); 
KhYuI_B : in STD_LOGIC_VECTOR (7 downto 0); 
KhYuI_P : out STD_LOGIC_VECTOR (15 downto 0)); 
end KhYuI_mult1; 

architecture Behavioral of KhYuI_mult1 is 
constant WIDTH: integer:=8; 
signal khyui_ua, khyui_bv0, khyui_bv1, khyui_bv2, khyui_bv3, khyui_bv4, khyui_bv5, khyui_bv6, khyui_bv7 : unsigned (WIDTH - 1 downto 0); 
signal khyui_bp, khyui_p0, khyui_p1, khyui_p2, khyui_p3, khyui_p4, khyui_p5, khyui_p6, khyui_p7 : unsigned (2*WIDTH - 1 downto 0); 
begin 
khyui_ua <= unsigned (KhYuI_A); 
khyui_bv0 <= (others => KhYuI_B(0)); 
khyui_bv1 <= (others => KhYuI_B(1)); 
khyui_bv2 <= (others => KhYuI_B(2)); 
khyui_bv3 <= (others => KhYuI_B(3)); 
khyui_bv4 <= (others => KhYuI_B(4)); 
khyui_bv5 <= (others => KhYuI_B(5)); 
khyui_bv6 <= (others => KhYuI_B(6)); 
khyui_bv7 <= (others => KhYuI_B(7)); 
khyui_p0 <= "00000000" & (khyui_bv0 and khyui_ua); 
khyui_p1 <= "0000000" & (khyui_bv1 and khyui_ua) & "0"; 
khyui_p2 <= "000000" & (khyui_bv2 and khyui_ua) & "00"; 
khyui_p3 <= "00000" & (khyui_bv3 and khyui_ua) & "000"; 
khyui_p4 <= "0000" & (khyui_bv4 and khyui_ua) & "0000"; 
khyui_p5 <= "000" & (khyui_bv5 and khyui_ua) & "00000"; 
khyui_p6 <= "00" & (khyui_bv6 and khyui_ua) & "000000"; 
khyui_p7 <= "0" & (khyui_bv7 and khyui_ua) & "0000000"; 
khyui_bp<=((khyui_p0+khyui_p1)+(khyui_p2+khyui_p3))+((khyui_p4+khyui_p5)+(khyui_p6+khyui_p7)); 
KhYuI_P<= std_logic_vector (khyui_bp); 
end Behavioral;