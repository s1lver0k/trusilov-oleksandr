-- Vhdl test bench created from schematic D:\fpga_labs\KhYuI-lab2\KhYuI_sch_all.sch - Sat Oct 15 23:30:24 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KhYuI_sch_all_KhYuI_sch_all_sch_tb IS
END KhYuI_sch_all_KhYuI_sch_all_sch_tb;
ARCHITECTURE behavioral OF KhYuI_sch_all_KhYuI_sch_all_sch_tb IS 

   COMPONENT KhYuI_sch_all
   PORT( KhYuI_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          KhYuI_B	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          KhYuI_P1	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KhYuI_P2	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KhYuI_P3	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL KhYuI_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL KhYuI_B	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL KhYuI_P1	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KhYuI_P2	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KhYuI_P3	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   constant clk_c : time := 10 ns;
	SIGNAL buf : integer :=  0;
BEGIN

   UUT: KhYuI_sch_all PORT MAP(
		KhYuI_A => KhYuI_A, 
		KhYuI_B => KhYuI_B, 
		KhYuI_P1 => KhYuI_P1, 
		KhYuI_P2 => KhYuI_P2, 
		KhYuI_P3 => KhYuI_P3
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
     KhYuI_A <= "00000011"; 
KhYuI_B <= "00000010"; 
buf <= buf+1; 
wait for clk_c; 
KhYuI_A <= "01100011"; 
KhYuI_B <= "00010010"; 
buf <= buf+1; 
wait for clk_c; 
KhYuI_A <= "11111111"; 
KhYuI_B <= "11111111"; 
buf <= buf+1; 
wait for clk_c; 

   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
