-- Vhdl test bench created from schematic D:\fpga_labs\kyi_lab4\kyi_sch_konv.sch - Fri Nov 18 00:11:23 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY kyi_sch_konv_kyi_sch_konv_sch_tb IS
END kyi_sch_konv_kyi_sch_konv_sch_tb;
ARCHITECTURE behavioral OF kyi_sch_konv_kyi_sch_konv_sch_tb IS 

   COMPONENT kyi_sch_konv
   PORT( c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          d	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CLK	:	IN	STD_LOGIC; 
          CLR	:	IN	STD_LOGIC; 
          ONE	:	IN	STD_LOGIC; 
          ZERO	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          res	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL d	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL b	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL CLR	:	STD_LOGIC:='0';
   SIGNAL ONE	:	STD_LOGIC:='1';
   SIGNAL ZERO	:	STD_LOGIC:='0';
   SIGNAL CE	:	STD_LOGIC:='1';
   SIGNAL res	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
	SIGNAL buf: integer := 3;
	constant clk_c : time := 10 ns;

BEGIN

   UUT: kyi_sch_konv PORT MAP(
		c => c, 
		d => d, 
		b => b, 
		CLK => CLK, 
		CLR => CLR, 
		ONE => ONE, 
		ZERO => ZERO, 
		CE => CE, 
		res => res
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      buf <= buf + 1;
      CLK <= '0';
		wait for clk_c/2;
   	c <= std_logic_vector(to_unsigned(buf,c'length));
		d <= std_logic_vector(to_unsigned(buf+2,d'length));
		b <= std_logic_vector(to_unsigned(buf+3,b'length));
		CLK <= '1';
		wait for clk_c/2;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
