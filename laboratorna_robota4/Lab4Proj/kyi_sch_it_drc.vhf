--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : kyi_sch_it_drc.vhf
-- /___/   /\     Timestamp : 11/19/2016 16:29:18
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64\unwrapped\sch2hdl.exe -sympath D:/fpga_labs/kyi_lab4/ipcore_dir -intstyle ise -family virtex4 -flat -suppress -vhdl kyi_sch_it_drc.vhf -w D:/fpga_labs/kyi_lab4/kyi_sch_it.sch
--Design Name: kyi_sch_it
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity kyi_sch_it is
   port ( b       : in    std_logic_vector (7 downto 0); 
          c       : in    std_logic_vector (7 downto 0); 
          CE      : in    std_logic; 
          CLK     : in    std_logic; 
          d       : in    std_logic_vector (7 downto 0); 
          Zero    : in    std_logic; 
          Control : out   std_logic_vector (23 downto 0); 
          Counter : out   std_logic_vector (3 downto 0); 
          RAM     : out   std_logic_vector (31 downto 0); 
          RAM1    : out   std_logic_vector (31 downto 0); 
          res     : out   std_logic_vector (31 downto 0));
end kyi_sch_it;

architecture BEHAVIORAL of kyi_sch_it is
   signal XLXN_6                  : std_logic_vector (31 downto 0);
   signal XLXN_7                  : std_logic_vector (31 downto 0);
   signal XLXN_8                  : std_logic_vector (31 downto 0);
   signal XLXN_9                  : std_logic_vector (31 downto 0);
   signal XLXN_10                 : std_logic_vector (31 downto 0);
   signal XLXN_21                 : std_logic_vector (31 downto 0);
   signal XLXN_22                 : std_logic_vector (31 downto 0);
   signal XLXN_23                 : std_logic_vector (31 downto 0);
   signal XLXN_24                 : std_logic_vector (31 downto 0);
   signal XLXN_54                 : std_logic_vector (31 downto 0);
   signal XLXN_55                 : std_logic_vector (31 downto 0);
   signal RAM_DUMMY               : std_logic_vector (31 downto 0);
   signal Control_DUMMY           : std_logic_vector (23 downto 0);
   signal Counter_DUMMY           : std_logic_vector (3 downto 0);
   signal RAM1_DUMMY              : std_logic_vector (31 downto 0);
   signal XLXI_1_CLK_openSignal   : std_logic;
   signal XLXI_3_Data4_openSignal : std_logic_vector (31 downto 0);
   component kyi_rom
      port ( CLK : in    std_logic; 
             A   : in    std_logic_vector (3 downto 0); 
             D   : out   std_logic_vector (23 downto 0));
   end component;
   
   component kyi_count
      port ( CLK : in    std_logic; 
             q   : out   std_logic_vector (3 downto 0));
   end component;
   
   component kyi_mul4_32
      port ( CLK      : in    std_logic; 
             s0       : in    std_logic_vector (1 downto 0); 
             Data1    : in    std_logic_vector (31 downto 0); 
             Data2    : in    std_logic_vector (31 downto 0); 
             Data3    : in    std_logic_vector (31 downto 0); 
             Data4    : in    std_logic_vector (31 downto 0); 
             Data_res : out   std_logic_vector (31 downto 0));
   end component;
   
   component kyi_ram_32
      port ( WE  : in    std_logic; 
             CE  : in    std_logic; 
             OE  : in    std_logic; 
             CLK : in    std_logic; 
             A   : in    std_logic_vector (3 downto 0); 
             DI  : in    std_logic_vector (31 downto 0); 
             DQ  : out   std_logic_vector (31 downto 0));
   end component;
   
   component kyi_reg_32
      port ( ld  : in    std_logic; 
             clk : in    std_logic; 
             clr : in    std_logic; 
             d   : in    std_logic_vector (31 downto 0); 
             q   : out   std_logic_vector (31 downto 0));
   end component;
   
   component bus_8_32
      port ( in_sig  : in    std_logic_vector (7 downto 0); 
             out_sig : out   std_logic_vector (31 downto 0));
   end component;
   
   component kyi_mux_32
      port ( s0     : in    std_logic; 
             CLK    : in    std_logic; 
             data_1 : in    std_logic_vector (31 downto 0); 
             data_2 : in    std_logic_vector (31 downto 0); 
             data_0 : out   std_logic_vector (31 downto 0));
   end component;
   
   component kyi_add32
      port ( a   : in    std_logic_vector (31 downto 0); 
             b   : in    std_logic_vector (31 downto 0); 
             clk : in    std_logic; 
             add : in    std_logic; 
             ce  : in    std_logic; 
             s   : out   std_logic_vector (31 downto 0));
   end component;
   
   component kyi_mul32
      port ( a   : in    std_logic_vector (31 downto 0); 
             b   : in    std_logic_vector (31 downto 0); 
             clk : in    std_logic; 
             p   : out   std_logic_vector (31 downto 0));
   end component;
   
begin
   Control(23 downto 0) <= Control_DUMMY(23 downto 0);
   Counter(3 downto 0) <= Counter_DUMMY(3 downto 0);
   RAM(31 downto 0) <= RAM_DUMMY(31 downto 0);
   RAM1(31 downto 0) <= RAM1_DUMMY(31 downto 0);
   XLXI_1 : kyi_rom
      port map (A(3 downto 0)=>Counter_DUMMY(3 downto 0),
                CLK=>XLXI_1_CLK_openSignal,
                D(23 downto 0)=>Control_DUMMY(23 downto 0));
   
   XLXI_2 : kyi_count
      port map (CLK=>CLK,
                q(3 downto 0)=>Counter_DUMMY(3 downto 0));
   
   XLXI_3 : kyi_mul4_32
      port map (CLK=>CLK,
                Data1(31 downto 0)=>XLXN_6(31 downto 0),
                Data2(31 downto 0)=>XLXN_9(31 downto 0),
                Data3(31 downto 0)=>XLXN_10(31 downto 0),
                Data4(31 downto 0)=>XLXI_3_Data4_openSignal(31 downto 0),
                s0(1 downto 0)=>Control_DUMMY(20 downto 19),
                Data_res(31 downto 0)=>XLXN_54(31 downto 0));
   
   XLXI_4 : kyi_mul4_32
      port map (CLK=>CLK,
                Data1(31 downto 0)=>XLXN_8(31 downto 0),
                Data2(31 downto 0)=>XLXN_7(31 downto 0),
                Data3(31 downto 0)=>XLXN_9(31 downto 0),
                Data4(31 downto 0)=>XLXN_10(31 downto 0),
                s0(1 downto 0)=>Control_DUMMY(13 downto 12),
                Data_res(31 downto 0)=>XLXN_55(31 downto 0));
   
   XLXI_5 : kyi_ram_32
      port map (A(3 downto 0)=>Control_DUMMY(17 downto 14),
                CE=>CE,
                CLK=>CLK,
                DI(31 downto 0)=>XLXN_54(31 downto 0),
                OE=>Control_DUMMY(18),
                WE=>Zero,
                DQ(31 downto 0)=>RAM1_DUMMY(31 downto 0));
   
   XLXI_6 : kyi_ram_32
      port map (A(3 downto 0)=>Control_DUMMY(10 downto 7),
                CE=>CE,
                CLK=>CLK,
                DI(31 downto 0)=>XLXN_55(31 downto 0),
                OE=>Control_DUMMY(11),
                WE=>Zero,
                DQ(31 downto 0)=>RAM_DUMMY(31 downto 0));
   
   XLXI_7 : kyi_reg_32
      port map (clk=>CLK,
                clr=>Control_DUMMY(5),
                d(31 downto 0)=>RAM_DUMMY(31 downto 0),
                ld=>Control_DUMMY(6),
                q(31 downto 0)=>res(31 downto 0));
   
   XLXI_8 : bus_8_32
      port map (in_sig(7 downto 0)=>b(7 downto 0),
                out_sig(31 downto 0)=>XLXN_7(31 downto 0));
   
   XLXI_9 : bus_8_32
      port map (in_sig(7 downto 0)=>c(7 downto 0),
                out_sig(31 downto 0)=>XLXN_6(31 downto 0));
   
   XLXI_10 : bus_8_32
      port map (in_sig(7 downto 0)=>d(7 downto 0),
                out_sig(31 downto 0)=>XLXN_8(31 downto 0));
   
   XLXI_11 : kyi_mux_32
      port map (CLK=>CLK,
                data_1(31 downto 0)=>RAM1_DUMMY(31 downto 0),
                data_2(31 downto 0)=>RAM_DUMMY(31 downto 0),
                s0=>Control_DUMMY(0),
                data_0(31 downto 0)=>XLXN_21(31 downto 0));
   
   XLXI_12 : kyi_mux_32
      port map (CLK=>CLK,
                data_1(31 downto 0)=>RAM1_DUMMY(31 downto 0),
                data_2(31 downto 0)=>RAM_DUMMY(31 downto 0),
                s0=>Control_DUMMY(1),
                data_0(31 downto 0)=>XLXN_22(31 downto 0));
   
   XLXI_13 : kyi_mux_32
      port map (CLK=>CLK,
                data_1(31 downto 0)=>RAM1_DUMMY(31 downto 0),
                data_2(31 downto 0)=>RAM_DUMMY(31 downto 0),
                s0=>Control_DUMMY(2),
                data_0(31 downto 0)=>XLXN_23(31 downto 0));
   
   XLXI_14 : kyi_mux_32
      port map (CLK=>CLK,
                data_1(31 downto 0)=>RAM1_DUMMY(31 downto 0),
                data_2(31 downto 0)=>RAM_DUMMY(31 downto 0),
                s0=>Control_DUMMY(3),
                data_0(31 downto 0)=>XLXN_24(31 downto 0));
   
   XLXI_15 : kyi_add32
      port map (a(31 downto 0)=>XLXN_23(31 downto 0),
                add=>Control_DUMMY(4),
                b(31 downto 0)=>XLXN_24(31 downto 0),
                ce=>CE,
                clk=>CLK,
                s(31 downto 0)=>XLXN_10(31 downto 0));
   
   XLXI_16 : kyi_mul32
      port map (a(31 downto 0)=>XLXN_21(31 downto 0),
                b(31 downto 0)=>XLXN_22(31 downto 0),
                clk=>CLK,
                p(31 downto 0)=>XLXN_9(31 downto 0));
   
end BEHAVIORAL;


