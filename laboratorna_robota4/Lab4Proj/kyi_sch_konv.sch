<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="c(7:0)" />
        <signal name="d(7:0)" />
        <signal name="b(7:0)" />
        <signal name="CLK" />
        <signal name="CLR" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_14(7:0)" />
        <signal name="XLXN_15(7:0)" />
        <signal name="XLXN_16(7:0)" />
        <signal name="XLXN_17(7:0)" />
        <signal name="XLXN_18(7:0)" />
        <signal name="XLXN_19(7:0)" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30(7:0)" />
        <signal name="XLXN_31(7:0)" />
        <signal name="ONE" />
        <signal name="ZERO" />
        <signal name="XLXN_34(7:0)" />
        <signal name="XLXN_35(7:0)" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="CE" />
        <signal name="XLXN_69(7:0)" />
        <signal name="XLXN_70(7:0)" />
        <signal name="XLXN_71(15:0)" />
        <signal name="XLXN_36(15:0)" />
        <signal name="XLXN_80(15:0)" />
        <signal name="XLXN_81" />
        <signal name="XLXN_82" />
        <signal name="XLXN_83(15:0)" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86(15:0)" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89(15:0)" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_92(15:0)" />
        <signal name="XLXN_93(15:0)" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_96" />
        <signal name="XLXN_97" />
        <signal name="XLXN_98" />
        <signal name="XLXN_99(15:0)" />
        <signal name="XLXN_100(15:0)" />
        <signal name="XLXN_101(15:0)" />
        <signal name="XLXN_102" />
        <signal name="XLXN_103" />
        <signal name="res(15:0)" />
        <signal name="XLXN_105(7:0)" />
        <signal name="XLXN_106(15:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Input" name="b(7:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CLR" />
        <port polarity="Input" name="ONE" />
        <port polarity="Input" name="ZERO" />
        <port polarity="Input" name="CE" />
        <port polarity="Output" name="res(15:0)" />
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="kyi_b2">
            <timestamp>2016-11-17T18:27:29</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="adsu8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="48" y1="-64" y2="-64" x1="128" />
            <line x2="128" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="kyi_mul">
            <timestamp>2016-11-17T18:31:20</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="kyi_mulc">
            <timestamp>2016-11-17T19:51:41</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_1">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="c(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_105(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_2">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="d(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_19(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_3">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="b(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_17(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="kyi_b2" name="XLXI_5">
            <blockpin signalname="XLXN_17(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_17(7:0)" name="b(7:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_106(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_6">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_18(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_30(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_7">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_19(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_31(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_8">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="c(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_35(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_106(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_36(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="adsu8" name="XLXI_10">
            <blockpin signalname="XLXN_31(7:0)" name="A(7:0)" />
            <blockpin signalname="ZERO" name="ADD" />
            <blockpin signalname="XLXN_30(7:0)" name="B(7:0)" />
            <blockpin signalname="ONE" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_34(7:0)" name="S(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_34(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_69(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_12">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_35(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_70(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="kyi_mul" name="XLXI_14">
            <blockpin signalname="XLXN_69(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_70(7:0)" name="b(7:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_92(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_36(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_93(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_15">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_93(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_100(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_16">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_92(15:0)" name="D(15:0)" />
            <blockpin signalname="XLXN_99(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_18">
            <blockpin signalname="XLXN_99(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_100(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_103" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_101(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_19">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_101(15:0)" name="D(15:0)" />
            <blockpin signalname="res(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_20">
            <blockpin signalname="XLXN_103" name="G" />
        </block>
        <block symbolname="kyi_mulc" name="XLXI_24">
            <blockpin signalname="XLXN_105(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_105(7:0)" name="b(7:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_18(7:0)" name="p(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="224" y="448" name="XLXI_1" orien="R0" />
        <branch name="c(7:0)">
            <wire x2="160" y1="192" y2="192" x1="112" />
            <wire x2="224" y1="192" y2="192" x1="160" />
            <wire x2="160" y1="16" y2="192" x1="160" />
            <wire x2="1440" y1="16" y2="16" x1="160" />
            <wire x2="1440" y1="16" y2="1136" x1="1440" />
            <wire x2="1536" y1="1136" y2="1136" x1="1440" />
        </branch>
        <iomarker fontsize="28" x="112" y="192" name="c(7:0)" orien="R180" />
        <iomarker fontsize="28" x="80" y="256" name="CE" orien="R180" />
        <iomarker fontsize="28" x="96" y="320" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="96" y="416" name="CLR" orien="R180" />
        <iomarker fontsize="28" x="112" y="672" name="d(7:0)" orien="R180" />
        <iomarker fontsize="28" x="112" y="1200" name="b(7:0)" orien="R180" />
        <instance x="1472" y="384" name="XLXI_6" orien="R0" />
        <instance x="1504" y="880" name="XLXI_7" orien="R0" />
        <branch name="XLXN_18(7:0)">
            <wire x2="1472" y1="128" y2="128" x1="1328" />
        </branch>
        <branch name="XLXN_19(7:0)">
            <wire x2="1056" y1="672" y2="672" x1="624" />
            <wire x2="1056" y1="624" y2="672" x1="1056" />
            <wire x2="1504" y1="624" y2="624" x1="1056" />
        </branch>
        <instance x="2032" y="576" name="XLXI_10" orien="R0" />
        <branch name="XLXN_30(7:0)">
            <wire x2="1936" y1="128" y2="128" x1="1856" />
            <wire x2="1936" y1="128" y2="384" x1="1936" />
            <wire x2="2032" y1="384" y2="384" x1="1936" />
        </branch>
        <branch name="XLXN_31(7:0)">
            <wire x2="1920" y1="624" y2="624" x1="1888" />
            <wire x2="1920" y1="256" y2="624" x1="1920" />
            <wire x2="2032" y1="256" y2="256" x1="1920" />
        </branch>
        <branch name="ONE">
            <wire x2="2032" y1="128" y2="128" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="2000" y="128" name="ONE" orien="R270" />
        <branch name="XLXN_35(7:0)">
            <wire x2="2288" y1="1136" y2="1136" x1="1920" />
            <wire x2="2288" y1="688" y2="1136" x1="2288" />
            <wire x2="2656" y1="688" y2="688" x1="2288" />
        </branch>
        <instance x="1536" y="1840" name="XLXI_9" orien="R0" />
        <branch name="ZERO">
            <wire x2="2032" y1="512" y2="512" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="2000" y="512" name="ZERO" orien="R90" />
        <instance x="2656" y="944" name="XLXI_12" orien="R0" />
        <branch name="XLXN_34(7:0)">
            <wire x2="2640" y1="320" y2="320" x1="2480" />
        </branch>
        <instance x="2640" y="576" name="XLXI_11" orien="R0" />
        <instance x="1536" y="1392" name="XLXI_8" orien="R0" />
        <instance x="784" y="1072" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_17(7:0)">
            <wire x2="704" y1="1200" y2="1200" x1="640" />
            <wire x2="704" y1="1200" y2="1216" x1="704" />
            <wire x2="784" y1="1216" y2="1216" x1="704" />
            <wire x2="704" y1="1152" y2="1200" x1="704" />
            <wire x2="784" y1="1152" y2="1152" x1="704" />
        </branch>
        <branch name="b(7:0)">
            <wire x2="256" y1="1200" y2="1200" x1="112" />
        </branch>
        <instance x="256" y="1456" name="XLXI_3" orien="R0" />
        <branch name="CLR">
            <wire x2="144" y1="416" y2="416" x1="96" />
            <wire x2="144" y1="416" y2="896" x1="144" />
            <wire x2="144" y1="896" y2="960" x1="144" />
            <wire x2="144" y1="960" y2="1424" x1="144" />
            <wire x2="256" y1="1424" y2="1424" x1="144" />
            <wire x2="144" y1="1424" y2="1808" x1="144" />
            <wire x2="1040" y1="1808" y2="1808" x1="144" />
            <wire x2="1392" y1="1808" y2="1808" x1="1040" />
            <wire x2="1536" y1="1808" y2="1808" x1="1392" />
            <wire x2="1040" y1="1808" y2="1888" x1="1040" />
            <wire x2="2528" y1="1888" y2="1888" x1="1040" />
            <wire x2="2592" y1="1888" y2="1888" x1="2528" />
            <wire x2="2592" y1="1888" y2="2512" x1="2592" />
            <wire x2="2720" y1="2512" y2="2512" x1="2592" />
            <wire x2="1456" y1="960" y2="960" x1="144" />
            <wire x2="1504" y1="960" y2="960" x1="1456" />
            <wire x2="240" y1="896" y2="896" x1="144" />
            <wire x2="224" y1="416" y2="416" x1="144" />
            <wire x2="1536" y1="1360" y2="1360" x1="1392" />
            <wire x2="1392" y1="1360" y2="1808" x1="1392" />
            <wire x2="1472" y1="352" y2="352" x1="1456" />
            <wire x2="1456" y1="352" y2="960" x1="1456" />
            <wire x2="1504" y1="848" y2="960" x1="1504" />
            <wire x2="2000" y1="1552" y2="1792" x1="2000" />
            <wire x2="2592" y1="1792" y2="1792" x1="2000" />
            <wire x2="2592" y1="1792" y2="1888" x1="2592" />
            <wire x2="2640" y1="544" y2="544" x1="2592" />
            <wire x2="2592" y1="544" y2="912" x1="2592" />
            <wire x2="2656" y1="912" y2="912" x1="2592" />
            <wire x2="2592" y1="912" y2="1424" x1="2592" />
            <wire x2="2592" y1="1424" y2="1792" x1="2592" />
            <wire x2="2640" y1="1424" y2="1424" x1="2592" />
            <wire x2="2640" y1="1424" y2="1488" x1="2640" />
            <wire x2="3056" y1="1424" y2="1424" x1="2640" />
            <wire x2="3056" y1="1424" y2="1488" x1="3056" />
        </branch>
        <branch name="d(7:0)">
            <wire x2="240" y1="672" y2="672" x1="112" />
        </branch>
        <instance x="240" y="928" name="XLXI_2" orien="R0" />
        <instance x="3488" y="736" name="XLXI_14" orien="R90">
        </instance>
        <branch name="XLXN_69(7:0)">
            <wire x2="3408" y1="320" y2="320" x1="3024" />
            <wire x2="3408" y1="320" y2="736" x1="3408" />
        </branch>
        <branch name="XLXN_70(7:0)">
            <wire x2="3344" y1="688" y2="688" x1="3040" />
            <wire x2="3344" y1="688" y2="736" x1="3344" />
        </branch>
        <branch name="CLK">
            <wire x2="160" y1="320" y2="320" x1="96" />
            <wire x2="160" y1="320" y2="544" x1="160" />
            <wire x2="160" y1="544" y2="800" x1="160" />
            <wire x2="160" y1="800" y2="928" x1="160" />
            <wire x2="160" y1="928" y2="1328" x1="160" />
            <wire x2="192" y1="1328" y2="1328" x1="160" />
            <wire x2="192" y1="1328" y2="1472" x1="192" />
            <wire x2="608" y1="1472" y2="1472" x1="192" />
            <wire x2="608" y1="1472" y2="1712" x1="608" />
            <wire x2="1104" y1="1712" y2="1712" x1="608" />
            <wire x2="1408" y1="1712" y2="1712" x1="1104" />
            <wire x2="1536" y1="1712" y2="1712" x1="1408" />
            <wire x2="1104" y1="1712" y2="1872" x1="1104" />
            <wire x2="2560" y1="1872" y2="1872" x1="1104" />
            <wire x2="2624" y1="1872" y2="1872" x1="2560" />
            <wire x2="2624" y1="1872" y2="2416" x1="2624" />
            <wire x2="2720" y1="2416" y2="2416" x1="2624" />
            <wire x2="688" y1="1472" y2="1472" x1="608" />
            <wire x2="256" y1="1328" y2="1328" x1="192" />
            <wire x2="1376" y1="928" y2="928" x1="160" />
            <wire x2="240" y1="800" y2="800" x1="160" />
            <wire x2="672" y1="544" y2="544" x1="160" />
            <wire x2="224" y1="320" y2="320" x1="160" />
            <wire x2="672" y1="288" y2="544" x1="672" />
            <wire x2="752" y1="288" y2="288" x1="672" />
            <wire x2="688" y1="1312" y2="1472" x1="688" />
            <wire x2="784" y1="1312" y2="1312" x1="688" />
            <wire x2="1376" y1="752" y2="928" x1="1376" />
            <wire x2="1392" y1="752" y2="752" x1="1376" />
            <wire x2="1504" y1="752" y2="752" x1="1392" />
            <wire x2="1472" y1="256" y2="256" x1="1392" />
            <wire x2="1392" y1="256" y2="752" x1="1392" />
            <wire x2="1408" y1="1264" y2="1712" x1="1408" />
            <wire x2="1536" y1="1264" y2="1264" x1="1408" />
            <wire x2="2000" y1="1456" y2="1456" x1="1968" />
            <wire x2="1968" y1="1456" y2="1696" x1="1968" />
            <wire x2="2560" y1="1696" y2="1696" x1="1968" />
            <wire x2="2560" y1="1696" y2="1872" x1="2560" />
            <wire x2="2640" y1="448" y2="448" x1="2560" />
            <wire x2="2560" y1="448" y2="816" x1="2560" />
            <wire x2="2656" y1="816" y2="816" x1="2560" />
            <wire x2="2560" y1="816" y2="1008" x1="2560" />
            <wire x2="2624" y1="1008" y2="1008" x1="2560" />
            <wire x2="2560" y1="1008" y2="1376" x1="2560" />
            <wire x2="2560" y1="1376" y2="1696" x1="2560" />
            <wire x2="2736" y1="1376" y2="1376" x1="2560" />
            <wire x2="2736" y1="1376" y2="1488" x1="2736" />
            <wire x2="3152" y1="1376" y2="1376" x1="2736" />
            <wire x2="3152" y1="1376" y2="1488" x1="3152" />
            <wire x2="2624" y1="560" y2="1008" x1="2624" />
            <wire x2="3248" y1="560" y2="560" x1="2624" />
            <wire x2="3248" y1="560" y2="736" x1="3248" />
        </branch>
        <branch name="CE">
            <wire x2="192" y1="256" y2="256" x1="80" />
            <wire x2="224" y1="256" y2="256" x1="192" />
            <wire x2="192" y1="256" y2="736" x1="192" />
            <wire x2="192" y1="736" y2="912" x1="192" />
            <wire x2="192" y1="912" y2="1264" x1="192" />
            <wire x2="256" y1="1264" y2="1264" x1="192" />
            <wire x2="1328" y1="912" y2="912" x1="192" />
            <wire x2="240" y1="736" y2="736" x1="192" />
            <wire x2="192" y1="1264" y2="1264" x1="128" />
            <wire x2="128" y1="1264" y2="1648" x1="128" />
            <wire x2="1088" y1="1648" y2="1648" x1="128" />
            <wire x2="1088" y1="1648" y2="1856" x1="1088" />
            <wire x2="2496" y1="1856" y2="1856" x1="1088" />
            <wire x2="2608" y1="1856" y2="1856" x1="2496" />
            <wire x2="2608" y1="1856" y2="2352" x1="2608" />
            <wire x2="2720" y1="2352" y2="2352" x1="2608" />
            <wire x2="1456" y1="1648" y2="1648" x1="1088" />
            <wire x2="1536" y1="1648" y2="1648" x1="1456" />
            <wire x2="1328" y1="688" y2="912" x1="1328" />
            <wire x2="1344" y1="688" y2="688" x1="1328" />
            <wire x2="1504" y1="688" y2="688" x1="1344" />
            <wire x2="1344" y1="192" y2="688" x1="1344" />
            <wire x2="1472" y1="192" y2="192" x1="1344" />
            <wire x2="1456" y1="1200" y2="1648" x1="1456" />
            <wire x2="1536" y1="1200" y2="1200" x1="1456" />
            <wire x2="2000" y1="1392" y2="1392" x1="1984" />
            <wire x2="1984" y1="1392" y2="1632" x1="1984" />
            <wire x2="2496" y1="1632" y2="1632" x1="1984" />
            <wire x2="2496" y1="1632" y2="1856" x1="2496" />
            <wire x2="2640" y1="384" y2="384" x1="2496" />
            <wire x2="2496" y1="384" y2="752" x1="2496" />
            <wire x2="2656" y1="752" y2="752" x1="2496" />
            <wire x2="2496" y1="752" y2="1296" x1="2496" />
            <wire x2="2496" y1="1296" y2="1632" x1="2496" />
            <wire x2="2800" y1="1296" y2="1296" x1="2496" />
            <wire x2="2800" y1="1296" y2="1488" x1="2800" />
            <wire x2="2912" y1="1296" y2="1296" x1="2800" />
            <wire x2="2912" y1="1296" y2="1344" x1="2912" />
            <wire x2="3216" y1="1344" y2="1344" x1="2912" />
            <wire x2="3216" y1="1344" y2="1488" x1="3216" />
        </branch>
        <branch name="XLXN_36(15:0)">
            <wire x2="1936" y1="1584" y2="1584" x1="1920" />
            <wire x2="2000" y1="1328" y2="1328" x1="1936" />
            <wire x2="1936" y1="1328" y2="1584" x1="1936" />
        </branch>
        <instance x="2000" y="1584" name="XLXI_13" orien="R0" />
        <instance x="2608" y="1488" name="XLXI_15" orien="R90" />
        <instance x="3024" y="1488" name="XLXI_16" orien="R90" />
        <instance x="2112" y="2528" name="XLXI_18" orien="R0" />
        <instance x="2720" y="2544" name="XLXI_19" orien="R0" />
        <branch name="XLXN_92(15:0)">
            <wire x2="3280" y1="1392" y2="1488" x1="3280" />
            <wire x2="3408" y1="1392" y2="1392" x1="3280" />
            <wire x2="3408" y1="1312" y2="1392" x1="3408" />
        </branch>
        <branch name="XLXN_93(15:0)">
            <wire x2="2864" y1="1328" y2="1328" x1="2384" />
            <wire x2="2864" y1="1328" y2="1488" x1="2864" />
        </branch>
        <branch name="XLXN_99(15:0)">
            <wire x2="2032" y1="2016" y2="2208" x1="2032" />
            <wire x2="2112" y1="2208" y2="2208" x1="2032" />
            <wire x2="3280" y1="2016" y2="2016" x1="2032" />
            <wire x2="3280" y1="1872" y2="2016" x1="3280" />
        </branch>
        <branch name="XLXN_100(15:0)">
            <wire x2="2096" y1="2032" y2="2336" x1="2096" />
            <wire x2="2112" y1="2336" y2="2336" x1="2096" />
            <wire x2="2864" y1="2032" y2="2032" x1="2096" />
            <wire x2="2864" y1="1872" y2="2032" x1="2864" />
        </branch>
        <branch name="XLXN_101(15:0)">
            <wire x2="2640" y1="2272" y2="2272" x1="2560" />
            <wire x2="2640" y1="2272" y2="2288" x1="2640" />
            <wire x2="2720" y1="2288" y2="2288" x1="2640" />
        </branch>
        <instance x="1824" y="2208" name="XLXI_20" orien="R0" />
        <branch name="XLXN_103">
            <wire x2="1888" y1="2000" y2="2080" x1="1888" />
            <wire x2="2112" y1="2000" y2="2000" x1="1888" />
            <wire x2="2112" y1="2000" y2="2080" x1="2112" />
        </branch>
        <branch name="res(15:0)">
            <wire x2="3120" y1="2288" y2="2288" x1="3104" />
            <wire x2="3200" y1="2288" y2="2288" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3200" y="2288" name="res(15:0)" orien="R0" />
        <branch name="XLXN_105(7:0)">
            <wire x2="624" y1="192" y2="192" x1="608" />
            <wire x2="672" y1="192" y2="192" x1="624" />
            <wire x2="752" y1="192" y2="192" x1="672" />
            <wire x2="672" y1="128" y2="192" x1="672" />
            <wire x2="752" y1="128" y2="128" x1="672" />
        </branch>
        <instance x="752" y="48" name="XLXI_24" orien="R0">
        </instance>
        <branch name="XLXN_106(15:0)">
            <wire x2="1376" y1="1152" y2="1152" x1="1360" />
            <wire x2="1376" y1="1152" y2="1584" x1="1376" />
            <wire x2="1536" y1="1584" y2="1584" x1="1376" />
        </branch>
    </sheet>
</drawing>