----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:52:07 11/19/2016 
-- Design Name: 
-- Module Name:    kyi_count - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kyi_count is
    Port ( CLK : in  STD_LOGIC;
           q : out  STD_LOGIC_VECTOR (3 downto 0));
end kyi_count;

architecture Behavioral of kyi_count is
signal cin : std_logic_vector(3 downto 0) :="0000";
begin
process(CLK)
    begin
        if cin = "1011" then
	       cin <= "0000";
	    else
	       cin <= cin + 1;
	    end if;
       q <= cin;
    end process;



end Behavioral;

