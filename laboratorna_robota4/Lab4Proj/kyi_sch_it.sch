<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="b(7:0)" />
        <signal name="c(7:0)" />
        <signal name="d(7:0)" />
        <signal name="XLXN_6(31:0)" />
        <signal name="XLXN_7(31:0)" />
        <signal name="XLXN_8(31:0)" />
        <signal name="XLXN_9(31:0)" />
        <signal name="XLXN_10(31:0)" />
        <signal name="XLXN_21(31:0)" />
        <signal name="XLXN_22(31:0)" />
        <signal name="XLXN_23(31:0)" />
        <signal name="XLXN_24(31:0)" />
        <signal name="CLK" />
        <signal name="CE" />
        <signal name="Zero" />
        <signal name="XLXN_54(31:0)" />
        <signal name="XLXN_55(31:0)" />
        <signal name="res(31:0)" />
        <signal name="Counter(3:0)" />
        <signal name="RAM1(31:0)" />
        <signal name="RAM2(31:0)" />
        <signal name="Control(23:0)" />
        <signal name="Control(20:19)" />
        <signal name="Control(18)" />
        <signal name="Control(17:14)" />
        <signal name="Control(13:12)" />
        <signal name="Control(11)" />
        <signal name="Control(10:7)" />
        <signal name="Control(5)" />
        <signal name="Control(6)" />
        <signal name="Control(4)" />
        <signal name="Control(3)" />
        <signal name="Control(2)" />
        <signal name="Control(1)" />
        <signal name="Control(0)" />
        <port polarity="Input" name="b(7:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="Zero" />
        <port polarity="Output" name="res(31:0)" />
        <port polarity="Output" name="Counter(3:0)" />
        <port polarity="Output" name="RAM1(31:0)" />
        <port polarity="Output" name="RAM2(31:0)" />
        <port polarity="Output" name="Control(23:0)" />
        <blockdef name="kyi_rom">
            <timestamp>2016-11-19T8:46:9</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="kyi_count">
            <timestamp>2016-11-19T8:53:40</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="kyi_mul4_32">
            <timestamp>2016-11-19T9:8:45</timestamp>
            <rect width="304" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-364" height="24" />
            <line x2="432" y1="-352" y2="-352" x1="368" />
        </blockdef>
        <blockdef name="kyi_ram_32">
            <timestamp>2016-11-19T8:50:7</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
        </blockdef>
        <blockdef name="kyi_reg_32">
            <timestamp>2016-11-19T8:57:7</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="bus_8_32">
            <timestamp>2016-11-19T8:59:19</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="kyi_mux_32">
            <timestamp>2016-11-19T9:3:9</timestamp>
            <rect width="288" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="352" y="-236" height="24" />
            <line x2="416" y1="-224" y2="-224" x1="352" />
        </blockdef>
        <blockdef name="kyi_add32">
            <timestamp>2016-11-19T9:14:42</timestamp>
            <rect width="224" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="112" y2="112" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" x1="0" />
            <line x2="32" y1="176" y2="176" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="256" y1="112" y2="112" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="kyi_mul32">
            <timestamp>2016-11-19T9:11:49</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="kyi_rom" name="XLXI_1">
            <blockpin name="CLK" />
            <blockpin signalname="Counter(3:0)" name="A(3:0)" />
            <blockpin signalname="Control(23:0)" name="D(23:0)" />
        </block>
        <block symbolname="kyi_count" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Counter(3:0)" name="q(3:0)" />
        </block>
        <block symbolname="kyi_mul4_32" name="XLXI_3">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(20:19)" name="s0(1:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="Data1(31:0)" />
            <blockpin signalname="XLXN_9(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_10(31:0)" name="Data3(31:0)" />
            <blockpin name="Data4(31:0)" />
            <blockpin signalname="XLXN_54(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="kyi_mul4_32" name="XLXI_4">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(13:12)" name="s0(1:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="Data1(31:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="Data2(31:0)" />
            <blockpin signalname="XLXN_9(31:0)" name="Data3(31:0)" />
            <blockpin signalname="XLXN_10(31:0)" name="Data4(31:0)" />
            <blockpin signalname="XLXN_55(31:0)" name="Data_res(31:0)" />
        </block>
        <block symbolname="kyi_ram_32" name="XLXI_5">
            <blockpin signalname="Zero" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(18)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(17:14)" name="A(3:0)" />
            <blockpin signalname="XLXN_54(31:0)" name="DI(31:0)" />
            <blockpin signalname="RAM1(31:0)" name="DQ(31:0)" />
        </block>
        <block symbolname="kyi_ram_32" name="XLXI_6">
            <blockpin signalname="Zero" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(11)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(10:7)" name="A(3:0)" />
            <blockpin signalname="XLXN_55(31:0)" name="DI(31:0)" />
            <blockpin signalname="RAM2(31:0)" name="DQ(31:0)" />
        </block>
        <block symbolname="kyi_reg_32" name="XLXI_7">
            <blockpin signalname="Control(6)" name="ld" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="Control(5)" name="clr" />
            <blockpin signalname="RAM2(31:0)" name="d(31:0)" />
            <blockpin signalname="res(31:0)" name="q(31:0)" />
        </block>
        <block symbolname="bus_8_32" name="XLXI_8">
            <blockpin signalname="b(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="bus_8_32" name="XLXI_9">
            <blockpin signalname="c(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="bus_8_32" name="XLXI_10">
            <blockpin signalname="d(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="kyi_mux_32" name="XLXI_11">
            <blockpin signalname="Control(0)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="RAM1(31:0)" name="data_1(31:0)" />
            <blockpin signalname="RAM2(31:0)" name="data_2(31:0)" />
            <blockpin signalname="XLXN_21(31:0)" name="data_0(31:0)" />
        </block>
        <block symbolname="kyi_mux_32" name="XLXI_12">
            <blockpin signalname="Control(1)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="RAM1(31:0)" name="data_1(31:0)" />
            <blockpin signalname="RAM2(31:0)" name="data_2(31:0)" />
            <blockpin signalname="XLXN_22(31:0)" name="data_0(31:0)" />
        </block>
        <block symbolname="kyi_mux_32" name="XLXI_13">
            <blockpin signalname="Control(2)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="RAM1(31:0)" name="data_1(31:0)" />
            <blockpin signalname="RAM2(31:0)" name="data_2(31:0)" />
            <blockpin signalname="XLXN_23(31:0)" name="data_0(31:0)" />
        </block>
        <block symbolname="kyi_mux_32" name="XLXI_14">
            <blockpin signalname="Control(3)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="RAM1(31:0)" name="data_1(31:0)" />
            <blockpin signalname="RAM2(31:0)" name="data_2(31:0)" />
            <blockpin signalname="XLXN_24(31:0)" name="data_0(31:0)" />
        </block>
        <block symbolname="kyi_add32" name="XLXI_15">
            <blockpin signalname="XLXN_23(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_24(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="Control(4)" name="add" />
            <blockpin signalname="CE" name="ce" />
            <blockpin signalname="XLXN_10(31:0)" name="s(31:0)" />
        </block>
        <block symbolname="kyi_mul32" name="XLXI_16">
            <blockpin signalname="XLXN_21(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_22(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_9(31:0)" name="p(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="112" y="720" name="XLXI_2" orien="R0">
        </instance>
        <instance x="704" y="1040" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1312" y="1040" name="XLXI_5" orien="R0">
        </instance>
        <instance x="1872" y="1040" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2544" y="1024" name="XLXI_6" orien="R0">
        </instance>
        <instance x="112" y="336" name="XLXI_1" orien="R0">
        </instance>
        <instance x="160" y="1264" name="XLXI_8" orien="R0">
        </instance>
        <instance x="160" y="1408" name="XLXI_9" orien="R0">
        </instance>
        <instance x="160" y="1552" name="XLXI_10" orien="R0">
        </instance>
        <instance x="560" y="1968" name="XLXI_11" orien="R0">
        </instance>
        <instance x="560" y="2368" name="XLXI_12" orien="R0">
        </instance>
        <instance x="1056" y="1744" name="XLXI_16" orien="R0">
        </instance>
        <branch name="b(7:0)">
            <wire x2="160" y1="1232" y2="1232" x1="128" />
        </branch>
        <iomarker fontsize="28" x="128" y="1232" name="b(7:0)" orien="R180" />
        <branch name="c(7:0)">
            <wire x2="160" y1="1376" y2="1376" x1="128" />
        </branch>
        <iomarker fontsize="28" x="128" y="1376" name="c(7:0)" orien="R180" />
        <branch name="d(7:0)">
            <wire x2="160" y1="1520" y2="1520" x1="128" />
        </branch>
        <iomarker fontsize="28" x="128" y="1520" name="d(7:0)" orien="R180" />
        <branch name="XLXN_6(31:0)">
            <wire x2="624" y1="1376" y2="1376" x1="544" />
            <wire x2="624" y1="816" y2="1376" x1="624" />
            <wire x2="704" y1="816" y2="816" x1="624" />
        </branch>
        <branch name="XLXN_7(31:0)">
            <wire x2="1760" y1="1232" y2="1232" x1="544" />
            <wire x2="1760" y1="880" y2="1232" x1="1760" />
            <wire x2="1872" y1="880" y2="880" x1="1760" />
        </branch>
        <branch name="XLXN_8(31:0)">
            <wire x2="1744" y1="1520" y2="1520" x1="544" />
            <wire x2="1744" y1="816" y2="1520" x1="1744" />
            <wire x2="1872" y1="816" y2="816" x1="1744" />
        </branch>
        <branch name="XLXN_9(31:0)">
            <wire x2="704" y1="880" y2="880" x1="640" />
            <wire x2="640" y1="880" y2="1152" x1="640" />
            <wire x2="1776" y1="1152" y2="1152" x1="640" />
            <wire x2="1824" y1="1152" y2="1152" x1="1776" />
            <wire x2="1776" y1="1152" y2="1824" x1="1776" />
            <wire x2="1776" y1="1824" y2="1824" x1="1632" />
            <wire x2="1872" y1="944" y2="944" x1="1824" />
            <wire x2="1824" y1="944" y2="1152" x1="1824" />
        </branch>
        <branch name="XLXN_10(31:0)">
            <wire x2="704" y1="944" y2="944" x1="656" />
            <wire x2="656" y1="944" y2="1120" x1="656" />
            <wire x2="1856" y1="1120" y2="1120" x1="656" />
            <wire x2="2944" y1="1120" y2="1120" x1="1856" />
            <wire x2="2944" y1="1120" y2="2000" x1="2944" />
            <wire x2="1872" y1="1008" y2="1008" x1="1856" />
            <wire x2="1856" y1="1008" y2="1120" x1="1856" />
            <wire x2="2944" y1="2000" y2="2000" x1="2896" />
        </branch>
        <branch name="XLXN_21(31:0)">
            <wire x2="1008" y1="1744" y2="1744" x1="976" />
            <wire x2="1008" y1="1744" y2="1824" x1="1008" />
            <wire x2="1056" y1="1824" y2="1824" x1="1008" />
        </branch>
        <branch name="XLXN_22(31:0)">
            <wire x2="1008" y1="2144" y2="2144" x1="976" />
            <wire x2="1008" y1="1888" y2="2144" x1="1008" />
            <wire x2="1056" y1="1888" y2="1888" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="304" y="1808" name="CLK" orien="R180" />
        <instance x="2016" y="1984" name="XLXI_13" orien="R0">
        </instance>
        <instance x="2048" y="2384" name="XLXI_14" orien="R0">
        </instance>
        <branch name="CE">
            <wire x2="1296" y1="464" y2="464" x1="832" />
            <wire x2="1296" y1="464" y2="752" x1="1296" />
            <wire x2="1312" y1="752" y2="752" x1="1296" />
            <wire x2="2368" y1="464" y2="464" x1="1296" />
            <wire x2="2368" y1="464" y2="736" x1="2368" />
            <wire x2="2544" y1="736" y2="736" x1="2368" />
            <wire x2="2368" y1="736" y2="1232" x1="2368" />
            <wire x2="2528" y1="1232" y2="1232" x1="2368" />
            <wire x2="2528" y1="1232" y2="2128" x1="2528" />
            <wire x2="2608" y1="2128" y2="2128" x1="2528" />
        </branch>
        <iomarker fontsize="28" x="832" y="464" name="CE" orien="R180" />
        <branch name="Zero">
            <wire x2="1264" y1="144" y2="144" x1="1136" />
            <wire x2="1264" y1="144" y2="688" x1="1264" />
            <wire x2="1312" y1="688" y2="688" x1="1264" />
            <wire x2="2528" y1="144" y2="144" x1="1264" />
            <wire x2="2528" y1="144" y2="672" x1="2528" />
            <wire x2="2544" y1="672" y2="672" x1="2528" />
        </branch>
        <branch name="XLXN_54(31:0)">
            <wire x2="1168" y1="688" y2="688" x1="1136" />
            <wire x2="1168" y1="688" y2="1008" x1="1168" />
            <wire x2="1312" y1="1008" y2="1008" x1="1168" />
        </branch>
        <branch name="XLXN_55(31:0)">
            <wire x2="2352" y1="688" y2="688" x1="2304" />
            <wire x2="2352" y1="688" y2="992" x1="2352" />
            <wire x2="2544" y1="992" y2="992" x1="2352" />
        </branch>
        <branch name="CLK">
            <wire x2="112" y1="688" y2="688" x1="48" />
            <wire x2="48" y1="688" y2="1184" x1="48" />
            <wire x2="592" y1="1184" y2="1184" x1="48" />
            <wire x2="1280" y1="1184" y2="1184" x1="592" />
            <wire x2="1808" y1="1184" y2="1184" x1="1280" />
            <wire x2="1856" y1="1184" y2="1184" x1="1808" />
            <wire x2="1856" y1="1184" y2="1824" x1="1856" />
            <wire x2="2016" y1="1824" y2="1824" x1="1856" />
            <wire x2="1856" y1="1824" y2="2032" x1="1856" />
            <wire x2="2608" y1="2032" y2="2032" x1="1856" />
            <wire x2="1856" y1="2032" y2="2224" x1="1856" />
            <wire x2="2048" y1="2224" y2="2224" x1="1856" />
            <wire x2="1856" y1="2224" y2="2416" x1="1856" />
            <wire x2="2512" y1="1184" y2="1184" x1="1856" />
            <wire x2="2528" y1="1184" y2="1184" x1="2512" />
            <wire x2="400" y1="1808" y2="1808" x1="304" />
            <wire x2="400" y1="1808" y2="2208" x1="400" />
            <wire x2="560" y1="2208" y2="2208" x1="400" />
            <wire x2="400" y1="2208" y2="2416" x1="400" />
            <wire x2="1040" y1="2416" y2="2416" x1="400" />
            <wire x2="1856" y1="2416" y2="2416" x1="1040" />
            <wire x2="560" y1="1808" y2="1808" x1="400" />
            <wire x2="592" y1="688" y2="1184" x1="592" />
            <wire x2="704" y1="688" y2="688" x1="592" />
            <wire x2="1056" y1="1984" y2="1984" x1="1040" />
            <wire x2="1040" y1="1984" y2="2416" x1="1040" />
            <wire x2="1312" y1="880" y2="880" x1="1280" />
            <wire x2="1280" y1="880" y2="1184" x1="1280" />
            <wire x2="1872" y1="688" y2="688" x1="1808" />
            <wire x2="1808" y1="688" y2="1184" x1="1808" />
            <wire x2="2544" y1="864" y2="864" x1="2512" />
            <wire x2="2512" y1="864" y2="1184" x1="2512" />
            <wire x2="2528" y1="1088" y2="1184" x1="2528" />
            <wire x2="3376" y1="1088" y2="1088" x1="2528" />
            <wire x2="3376" y1="1088" y2="1120" x1="3376" />
        </branch>
        <branch name="res(31:0)">
            <wire x2="3440" y1="1504" y2="1520" x1="3440" />
            <wire x2="3440" y1="1520" y2="1696" x1="3440" />
        </branch>
        <iomarker fontsize="28" x="1136" y="144" name="Zero" orien="R180" />
        <branch name="Counter(3:0)">
            <wire x2="112" y1="304" y2="304" x1="32" />
            <wire x2="32" y1="304" y2="560" x1="32" />
            <wire x2="512" y1="560" y2="560" x1="32" />
            <wire x2="512" y1="560" y2="688" x1="512" />
            <wire x2="512" y1="688" y2="688" x1="496" />
            <wire x2="512" y1="480" y2="560" x1="512" />
        </branch>
        <iomarker fontsize="28" x="512" y="480" name="Counter(3:0)" orien="R270" />
        <branch name="RAM1(31:0)">
            <wire x2="512" y1="1648" y2="1872" x1="512" />
            <wire x2="560" y1="1872" y2="1872" x1="512" />
            <wire x2="512" y1="1872" y2="2272" x1="512" />
            <wire x2="560" y1="2272" y2="2272" x1="512" />
            <wire x2="1712" y1="1648" y2="1648" x1="512" />
            <wire x2="1712" y1="1648" y2="1888" x1="1712" />
            <wire x2="2016" y1="1888" y2="1888" x1="1712" />
            <wire x2="1712" y1="1888" y2="2288" x1="1712" />
            <wire x2="2048" y1="2288" y2="2288" x1="1712" />
            <wire x2="1712" y1="688" y2="688" x1="1696" />
            <wire x2="1712" y1="688" y2="1376" x1="1712" />
            <wire x2="1808" y1="1376" y2="1376" x1="1712" />
            <wire x2="1712" y1="1376" y2="1648" x1="1712" />
        </branch>
        <iomarker fontsize="28" x="1808" y="1376" name="RAM1(31:0)" orien="R0" />
        <branch name="RAM2(31:0)">
            <wire x2="464" y1="1600" y2="1936" x1="464" />
            <wire x2="464" y1="1936" y2="2336" x1="464" />
            <wire x2="560" y1="2336" y2="2336" x1="464" />
            <wire x2="560" y1="1936" y2="1936" x1="464" />
            <wire x2="1936" y1="1600" y2="1600" x1="464" />
            <wire x2="1936" y1="1600" y2="1952" x1="1936" />
            <wire x2="2016" y1="1952" y2="1952" x1="1936" />
            <wire x2="1936" y1="1952" y2="2352" x1="1936" />
            <wire x2="2048" y1="2352" y2="2352" x1="1936" />
            <wire x2="2976" y1="1600" y2="1600" x1="1936" />
            <wire x2="3184" y1="1600" y2="1600" x1="2976" />
            <wire x2="2976" y1="672" y2="672" x1="2928" />
            <wire x2="2976" y1="672" y2="1056" x1="2976" />
            <wire x2="2976" y1="1056" y2="1600" x1="2976" />
            <wire x2="3248" y1="1056" y2="1056" x1="2976" />
            <wire x2="3248" y1="1056" y2="1120" x1="3248" />
        </branch>
        <branch name="XLXN_24(31:0)">
            <wire x2="2592" y1="2160" y2="2160" x1="2464" />
            <wire x2="2592" y1="2000" y2="2160" x1="2592" />
            <wire x2="2608" y1="2000" y2="2000" x1="2592" />
        </branch>
        <branch name="XLXN_23(31:0)">
            <wire x2="2592" y1="1760" y2="1760" x1="2432" />
            <wire x2="2592" y1="1760" y2="1968" x1="2592" />
            <wire x2="2608" y1="1968" y2="1968" x1="2592" />
        </branch>
        <instance x="2608" y="1888" name="XLXI_15" orien="R0">
        </instance>
        <iomarker fontsize="28" x="3184" y="1600" name="RAM2(31:0)" orien="R0" />
        <branch name="Control(23:0)">
            <wire x2="448" y1="2640" y2="2640" x1="320" />
            <wire x2="480" y1="2640" y2="2640" x1="448" />
            <wire x2="1984" y1="2640" y2="2640" x1="480" />
            <wire x2="2032" y1="2640" y2="2640" x1="1984" />
            <wire x2="2544" y1="2640" y2="2640" x1="2032" />
            <wire x2="2992" y1="2640" y2="2640" x1="2544" />
            <wire x2="624" y1="240" y2="240" x1="496" />
            <wire x2="1200" y1="240" y2="240" x1="624" />
            <wire x2="1232" y1="240" y2="240" x1="1200" />
            <wire x2="1728" y1="240" y2="240" x1="1232" />
            <wire x2="1728" y1="240" y2="288" x1="1728" />
            <wire x2="1728" y1="288" y2="320" x1="1728" />
            <wire x2="1728" y1="320" y2="352" x1="1728" />
            <wire x2="1728" y1="352" y2="416" x1="1728" />
            <wire x2="2992" y1="416" y2="416" x1="1728" />
            <wire x2="2992" y1="416" y2="496" x1="2992" />
            <wire x2="2992" y1="496" y2="544" x1="2992" />
            <wire x2="2992" y1="544" y2="2640" x1="2992" />
        </branch>
        <bustap x2="624" y1="240" y2="336" x1="624" />
        <branch name="Control(20:19)">
            <wire x2="624" y1="336" y2="752" x1="624" />
            <wire x2="704" y1="752" y2="752" x1="624" />
        </branch>
        <bustap x2="1200" y1="240" y2="336" x1="1200" />
        <branch name="Control(18)">
            <wire x2="1200" y1="336" y2="816" x1="1200" />
            <wire x2="1312" y1="816" y2="816" x1="1200" />
        </branch>
        <bustap x2="1232" y1="240" y2="336" x1="1232" />
        <branch name="Control(17:14)">
            <wire x2="1232" y1="336" y2="944" x1="1232" />
            <wire x2="1312" y1="944" y2="944" x1="1232" />
        </branch>
        <bustap x2="1824" y1="288" y2="288" x1="1728" />
        <bustap x2="1824" y1="320" y2="320" x1="1728" />
        <bustap x2="1824" y1="352" y2="352" x1="1728" />
        <branch name="Control(13:12)">
            <wire x2="1840" y1="352" y2="352" x1="1824" />
            <wire x2="1840" y1="352" y2="752" x1="1840" />
            <wire x2="1872" y1="752" y2="752" x1="1840" />
        </branch>
        <branch name="Control(11)">
            <wire x2="2336" y1="320" y2="320" x1="1824" />
            <wire x2="2336" y1="320" y2="800" x1="2336" />
            <wire x2="2544" y1="800" y2="800" x1="2336" />
        </branch>
        <branch name="Control(10:7)">
            <wire x2="2320" y1="288" y2="288" x1="1824" />
            <wire x2="2320" y1="288" y2="928" x1="2320" />
            <wire x2="2544" y1="928" y2="928" x1="2320" />
        </branch>
        <iomarker fontsize="28" x="320" y="2640" name="Control(23:0)" orien="R180" />
        <instance x="3216" y="1120" name="XLXI_7" orien="R90">
        </instance>
        <iomarker fontsize="28" x="3440" y="1696" name="res(31:0)" orien="R90" />
        <bustap x2="3088" y1="496" y2="496" x1="2992" />
        <bustap x2="3088" y1="544" y2="544" x1="2992" />
        <bustap x2="448" y1="2640" y2="2544" x1="448" />
        <bustap x2="480" y1="2640" y2="2544" x1="480" />
        <bustap x2="2032" y1="2640" y2="2544" x1="2032" />
        <bustap x2="1984" y1="2640" y2="2544" x1="1984" />
        <bustap x2="2544" y1="2640" y2="2544" x1="2544" />
        <branch name="Control(5)">
            <wire x2="3312" y1="544" y2="544" x1="3088" />
            <wire x2="3312" y1="544" y2="1120" x1="3312" />
        </branch>
        <branch name="Control(6)">
            <wire x2="3440" y1="496" y2="496" x1="3088" />
            <wire x2="3440" y1="496" y2="1120" x1="3440" />
        </branch>
        <branch name="Control(4)">
            <wire x2="2608" y1="2064" y2="2064" x1="2544" />
            <wire x2="2544" y1="2064" y2="2544" x1="2544" />
        </branch>
        <branch name="Control(3)">
            <wire x2="2048" y1="2160" y2="2160" x1="2032" />
            <wire x2="2032" y1="2160" y2="2544" x1="2032" />
        </branch>
        <branch name="Control(2)">
            <wire x2="2016" y1="1760" y2="1760" x1="1984" />
            <wire x2="1984" y1="1760" y2="2544" x1="1984" />
        </branch>
        <branch name="Control(1)">
            <wire x2="560" y1="2144" y2="2144" x1="480" />
            <wire x2="480" y1="2144" y2="2544" x1="480" />
        </branch>
        <branch name="Control(0)">
            <wire x2="560" y1="1744" y2="1744" x1="448" />
            <wire x2="448" y1="1744" y2="2544" x1="448" />
        </branch>
    </sheet>
</drawing>