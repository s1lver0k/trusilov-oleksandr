-- Vhdl test bench created from schematic D:\fpga_labs\kyi_lab4\kyi_sch_it.sch - Sat Nov 19 16:29:58 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY kyi_sch_it_kyi_sch_it_sch_tb IS
END kyi_sch_it_kyi_sch_it_sch_tb;
ARCHITECTURE behavioral OF kyi_sch_it_kyi_sch_it_sch_tb IS 

   COMPONENT kyi_sch_it
   PORT( b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          d	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          CLK	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          Zero	:	IN	STD_LOGIC; 
          res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          Counter	:	OUT	STD_LOGIC_VECTOR (3 DOWNTO 0); 
          RAM1	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          RAM2	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          Control	:	OUT	STD_LOGIC_VECTOR (23 DOWNTO 0));
   END COMPONENT;

	SIGNAL RAM1	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL RAM2	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL Control	:	STD_LOGIC_VECTOR (23 DOWNTO 0);
   SIGNAL CE	:	STD_LOGIC:='1';
   SIGNAL ZERO	:	STD_LOGIC:='0';
   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"03";
   SIGNAL d	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"1F";
   SIGNAL b	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"0D";
   SIGNAL res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL Counter	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
	constant clk_c : time := 10 ns;

BEGIN

   UUT: kyi_sch_it PORT MAP(
		b => b, 
		c => c, 
		d => d, 
		CLK => CLK, 
		CE => CE, 
		Zero => Zero, 
		res => res, 
		Counter => Counter, 
		RAM1 => RAM1, 
		RAM2 => RAM2, 
		Control => Control
   );
CLC_process :process

	begin
	CLK <= '0';
	wait for clk_c/2;
		
	CLK <= '1';
	wait for clk_c/2;

	end process;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
