----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:28:02 11/18/2016 
-- Design Name: 
-- Module Name:    rom - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rom is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (23 downto 0);
           CLK : in  STD_LOGIC);
end rom;

architecture Behavioral of rom is

begin
process (A)
		variable A_temp:integer;
	begin	   
		A_temp:= conv_integer(A(3 downto 0));	
				case A_temp is
					when 0 => D <= x"000050";	   				   				
					when 1 => D <= x"040050";	   				   				
					when 2 => D <= x"0850D0";     
					when 3 => D <= x"0008D3";     
					when 4 => D <= x"002150";
					when 5 => D <= x"044844";
					when 6 => D <= x"0031D0";  	  			
					when 7 => D <= x"0409D2";  	  			
					when 8 => D <= x"090000";  	  			
					when 9 => D <= x"050954";				
					when 10 => D <=x"0032D0";
					when 11 => D <=x"000AD0";
					when others => D <= "ZZZZZZZZZZZZZZZZZZZZZZZZ";
				end case;	

end Behavioral;

